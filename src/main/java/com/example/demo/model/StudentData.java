package com.example.demo.model;

import java.util.List;
import java.util.Objects;


public class StudentData {

    private Long id;

    private String imie;
    private String nazwisko;
    private boolean pelnoletni;

    public StudentData() {

    }

    private double wzrost;

    public StudentData(Long id, String imie, String nazwisko, boolean pelnoletni, double wzrost, List<GradeData> gradeList) {

        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pelnoletni = pelnoletni;
        this.wzrost = wzrost;
        this.gradeList = gradeList;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getImie() {

        return imie;
    }

    public void setImie(String imie) {

        this.imie = imie;
    }

    public String getNazwisko() {

        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {

        this.nazwisko = nazwisko;
    }

    public boolean isPelnoletni() {

        return pelnoletni;
    }

    public void setPelnoletni(boolean pelnoletni) {

        this.pelnoletni = pelnoletni;
    }

    public double getWzrost() {

        return wzrost;
    }

    public void setWzrost(double wzrost) {

        this.wzrost = wzrost;
    }

    public List<GradeData> getGradeList() {

        return gradeList;
    }

    public void setGradeList(List<GradeData> gradeList) {

        this.gradeList = gradeList;
    }

    private List<GradeData> gradeList;

    @Override public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof StudentData))
            return false;
        StudentData student = (StudentData) o;
        return Objects.equals(getId(), student.getId());
    }

    @Override public int hashCode() {

        return Objects.hash(getId());
    }
}
