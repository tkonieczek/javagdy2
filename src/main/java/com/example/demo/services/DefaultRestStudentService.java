package com.example.demo.services;

import com.example.demo.model.StudentData;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class DefaultRestStudentService implements StudentsRestService{

    List<StudentData> students;

    public DefaultRestStudentService() {
        students = new ArrayList<>();
        students.add(new StudentData(1L, "Mariusz", "Januszewski", true, 182, null));
        students.add(new StudentData(2L, "Sebastian", "Kowalski", true, 183, null));
        students.add(new StudentData(3L, "Janusz", "Nowak", true, 178, null));
        students.add(new StudentData(4L, "Grażyna", "Januszewski", true, 167, null));
        students.add(new StudentData(5L, "Mariusz", "Januszewski", true, 161, null));
    }


    @Override public Optional<StudentData> getStudentById(String studentId) {
        for(StudentData studentData : students) {
            if(studentData.getId().equals(Long.valueOf(studentId))) {
                return Optional.of(studentData);
            }
            Optional.empty();
        }
        return Optional.empty();
    }

    @Override public List<StudentData> getStudents() {

        return students;
    }

    @Override public boolean deleteStudent(String studentId) {

        Optional<StudentData> studentById = getStudentById(studentId);
        if(studentById.isPresent()) {
            return students.remove(studentById.get());
        }
        return false;
    }

    @Override public void addStudent(StudentData studentData) {
        Optional<StudentData> max = students.stream().max(Comparator.comparing(StudentData::getId));
        Long newId = max.map(StudentData::getId).orElse(1L) + 1;
        studentData.setId(newId);
        students.add(studentData);
        //return student;
        //students.add(studentData);
    }

    @Override public void updateStudent(String studentId, StudentData updatedStudent) {
        Optional<StudentData> currentStudent = getStudentById(studentId);
        currentStudent.ifPresent(current -> {
            current.setImie(updatedStudent.getImie());
            current.setNazwisko(updatedStudent.getNazwisko());
            current.setWzrost(updatedStudent.getWzrost());
            current.setPelnoletni(updatedStudent.isPelnoletni());
        });
    }
}
